//
//  main.m
//  IJKMediaFramework
//
//  Created by Sun on 01/03/2020.
//  Copyright (c) 2020 QingClass. All rights reserved.
//

@import UIKit;
#import "IJKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IJKAppDelegate class]));
    }
}
