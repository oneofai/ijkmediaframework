//
//  IJKViewController.m
//  IJKMediaFramework
//
//  Created by Sun on 01/03/2020.
//  Copyright (c) 2020 QingClass. All rights reserved.
//

#import "IJKViewController.h"
#import <IJKMediaFramework/IJKMediaFramework.h>

@interface IJKViewController ()

@property (nonatomic, strong) NSURL *assetURL;
//@property (nonatomic, strong) IJKAVMoviePlayerController *player;

@property (nonatomic, strong) IJKFFMoviePlayerController *player;

@end

@implementation IJKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.assetURL = [NSURL URLWithString:@"https://www.apple.com.cn/105/media/cn/iphone-11/2019/dc09a167-9d96-4ea8-935e-14af260ac4b1/films/product/iphone-11-product-tpl-cn-2019_1280x720h.mp4"];
    
    [IJKFFMoviePlayerController setLogReport:YES];
    [IJKFFMoviePlayerController setLogLevel:k_IJK_LOG_DEBUG];
    
    // IJKplayer属性参数设置
    IJKFFOptions *options = [IJKFFOptions optionsByDefault];
    
    self.player = [[IJKFFMoviePlayerController alloc] initWithContentURL:self.assetURL withOptions:options];
    [self.player prepareToPlay];
    self.player.scalingMode = IJKMPMovieScalingModeAspectFill;
    self.player.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.player.view.backgroundColor = [UIColor blackColor];
    self.player.shouldAutoplay = YES;
    CGFloat width = CGRectGetWidth(self.view.bounds);
    CGFloat height = width * 9 / 16;
    self.player.view.frame = CGRectMake(0, (CGRectGetHeight(self.view.bounds) - height) / 2, width, height);
    [self.view addSubview:self.player.view];
}

@end
